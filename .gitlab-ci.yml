include:
  - template: Security/SAST.gitlab-ci.yml

sast:
  stage: testing

stages:
  - linting
  - preparation
  - building
  - testing
  - security
  - deploy

image: registry.gitlab.com/joshuaroytino/laravel-8-blog:latest

# Variables
variables:
  MYSQL_ROOT_PASSWORD: root
  MYSQL_USER: mysql_user
  MYSQL_PASSWORD: mysql_password
  MYSQL_DATABASE: mysql_db
  DB_HOST: mysql

cache:
  key: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG"

pint:
  stage: linting
  script:
    - pint --test
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

composer:
  stage: preparation
  script:
    - php -v
    - composer install --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts
    - cp .env.gitlab .env
    - cp .env.gitlab .env.testing
    - php artisan key:generate
  artifacts:
    paths:
      - vendor/
      - .env
    expire_in: 1 days
    when: always
  cache:
    paths:
      - vendor/
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

npm:
  stage: preparation
  script:
    - npm --version
    - npm ci
  artifacts:
    paths:
      - node_modules/
    expire_in: 1 days
    when: always
  cache:
    paths:
      - node_modules/
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

build-assets:
  stage: building
  # Download the artifacts for these jobs
  dependencies:
    - composer
    - npm
  script:
    - npm --version
    - npm run production
  artifacts:
    paths:
      - public/css/
      - public/js/
      - public/mix-manifest.json
    expire_in: 1 days
    when: always
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

db-seeding:
  stage: building
  services:
    - name: mysql:8.0
      command: [ "--default-authentication-plugin=mysql_native_password" ]
  # Download the artifacts for these jobs
  dependencies:
    - composer
    - npm
  script:
    - mysql --version
    - php artisan migrate:fresh --seed
  artifacts:
    paths:
      - storage/logs # for debugging
    expire_in: 1 days
    when: always
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

phpunit:
  stage: testing
  services:
    - name: mysql:8.0
      command: [ "--default-authentication-plugin=mysql_native_password" ]
  # Download the artifacts for these jobs
  dependencies:
    - build-assets
    - composer
    - db-seeding
  script:
    - php -v
    - ./vendor/phpunit/phpunit/phpunit --version
    - php artisan config:clear
    - php artisan test
  artifacts:
    paths:
      - ./storage/logs # for debugging
    expire_in: 1 days
    when: on_failure
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

#codestyle:
#  stage: testing
#  image: lorisleiva/laravel-docker
#  script:
#    - phpcs --extensions=php app
#  dependencies: [ ]

phpcpd:
  stage: testing
  script:
    - test -f phpcpd.phar || curl -L https://phar.phpunit.de/phpcpd.phar -o phpcpd.phar
    - php phpcpd.phar app/ --min-lines=50
  dependencies: [ ]
  cache:
    paths:
      - phpcpd.phar
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

php-security-checker:
  stage: security
  image: registry.gitlab.com/pipeline-components/php-security-checker:latest
  script:
    - cd ${COMPOSER_LOCATION:-.} && security-checker security:check composer.lock
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

deploy_production:
  stage: deploy
  dependencies:
    - composer
  script:
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - ssh-add <(echo "$SSH_PRIVATE_KEY")
    - mkdir -p ~/.ssh
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'

    # Run Envoy
    - ~/.composer/vendor/bin/envoy run deploy
  environment:
    name: production
    url: https://laravel-blog.devjt.com
  when: manual
  only:
    - master
