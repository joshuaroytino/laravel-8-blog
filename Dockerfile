FROM lorisleiva/laravel-docker:8.1

# Install Laravel Envoy
RUN composer global require "laravel/envoy=~1.0"
RUN composer global require "laravel/pint=~1.0" --dev
