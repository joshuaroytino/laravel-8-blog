@servers(['web' => 'deployer@128.199.84.141'])

@setup
$repository = 'git@gitlab.com:joshuaroytino/laravel-8-blog.git';
$releases_dir = '/var/www/laravel-blog.devjt.com/releases';
$app_dir = '/var/www/laravel-blog.devjt.com';
$release = date('YmdHis');
$new_release_dir = $releases_dir .'/'. $release;
@endsetup

@story('deploy')
clone_repository
run_dependencies
update_symlinks
optimize
migrate_database
clean_old_releases
@endstory

@task('clone_repository')
echo 'Cloning repository'
[ -d {{ $releases_dir }} ] || mkdir -p {{ $releases_dir }}
[ -d {{ $app_dir }}/storage ] || mkdir {{ $app_dir }}/storage;
[ -d {{ $app_dir }}/storage/framework/logs ] || mkdir -p {{ $app_dir }}/storage/framework/logs;
[ -d {{ $app_dir }}/storage/framework/cache/data ] || mkdir -p {{ $app_dir }}/storage/framework/cache/data;
[ -d {{ $app_dir }}/storage/framework/sessions ] || mkdir -p {{ $app_dir }}/storage/framework/sessions;
[ -d {{ $app_dir }}/storage/framework/views ] || mkdir -p {{ $app_dir }}/storage/framework/views;

git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
cd {{ $new_release_dir }}
git reset --hard {{ $commit }}
@endtask

@task('run_dependencies')
echo "Starting deployment ({{ $release }})"
cd {{ $new_release_dir }}
echo "Installing composer dependencies"
composer install --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts --no-dev
echo "Installing npm dependencies"
npm ci
npm run production
@endtask

@task('update_symlinks')
echo "Linking storage directory"
rm -rf {{ $new_release_dir }}/storage
ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage

echo 'Linking .env file'
ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env

echo 'Linking current release'
ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current
cd {{ $new_release_dir }}
php artisan storage:link
@endtask

@task('optimize')
echo "Starting optimize";
cd {{ $new_release_dir }}
php artisan cache:clear
php artisan config:cache
php artisan route:cache
php artisan view:cache
php artisan event:cache
@endtask

@task('migrate_database')
echo "🙈  Migrating database..."
cd {{ $new_release_dir }};
php artisan migrate --force
@endtask

@task('clean_old_releases')
echo "🚾  Cleaning up old releases...";
# Delete all but the 5 most recent.
cd {{ $releases_dir }}
ls -dt {{ $releases_dir }}/* | tail -n +6 | xargs -d "\n" rm -rf;
@endtask
