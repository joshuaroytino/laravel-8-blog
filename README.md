## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and
creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in
many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache)
  storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Running the Application (using Docker)

- Copy .env.example to .env . For the database variables, please note of the following:

```
DB_HOST = laravel_blog_db #This is the database container in the docker-compose.yml. Do NOT change.
DB_DATABASE = laravel_blog #This is the database name to be created under .docker/mysql. Do NOT change.
DB_USERNAME = root
DB_PASSWORD = secret #Required and can be any string.
```

- Build the container

```
docker-compose up -d --build
```

- Run the following commands. The container name of the app is called laravel_blog_app

```
docker exec -it laravel_blog_app composer install
docker exec -it laravel_blog_app php artisan key:generate
docker exec -it laravel_blog_app php artisan migrate:fresh --seed
```

- Visit <code>localhost</code>.

- Then, go to the registration page. Make sure to set the username to **admin**.

## Testing

- Copy .env.example to .env.testing. For the database variable, copy from .env except for DB_DATABASE

```
DB_DATABASE = laravel_blog_testing
```

- To run the tests.

```
docker exec -it laravel_blog_app php artisan test
```

## Connecting to the Database through a Client

- Connect to the database by supplying <code>host.docker.internal</code> to the Hostname/IP
- If the client is prompting <code>Access denied .... </code>, run this command:

```
docker exec -it laravel_blog_app php artisan db:show
```

## Before Adding a Commit

- Run the following commands before adding a commit:

```
docker exec -it laravel_blog_app ./vendor/bin/pint
docker exec -it laravel_blog_app php artisan test
```

- Make sure you are getting <code>PASS</code> remarks to both commands.

## Running Without Docker

- Read the commands above but run without the <code>docker exec -it laravel_blog_app</code>
