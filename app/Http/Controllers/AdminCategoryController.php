<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Models\Category;

class AdminCategoryController extends Controller
{
    public function index()
    {
        return view('admin.categories.index', [
            'categories' => Category::latest()
                ->withCount('posts')
                ->paginate(50),
        ]);
    }

    public function create(Category $category)
    {
        return view('admin.categories.create', [
            'category' => $category,
        ]);
    }

    public function store(StoreCategoryRequest $request)
    {
        Category::create($request->validated());

        return back()->with(['success' => 'A new category has been saved.']);
    }

    public function edit(Category $category)
    {
        return view('admin.categories.edit', [
            'category' => $category,
        ]);
    }

    public function update(Category $category, UpdateCategoryRequest $request)
    {
        $category->update($request->validated());
        $category = $category->refresh();

        return redirect(route('admin.categories.edit', $category))
            ->with(['success' => 'Category has been updated.']);
    }

    public function destroy(Category $category)
    {
        if ($category->posts()->count() > 0) {
            return back()
                ->with(['error' => 'Category has posts.']);
        }

        $category->deleteOrFail();

        return back()
            ->with(['success' => 'Category has been deleted.']);
    }
}
