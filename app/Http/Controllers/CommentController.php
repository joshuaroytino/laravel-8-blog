<?php

namespace App\Http\Controllers;

use App\Models\Post;

class CommentController extends Controller
{
    public function store(Post $post)
    {
        $validatedData = request()->validate([
            'body' => 'required|string',
        ]);

        $post->comments()->create([
            'user_id' => auth()->id(),
            'body' => $validatedData['body'],
        ]);

        return back();
    }
}
