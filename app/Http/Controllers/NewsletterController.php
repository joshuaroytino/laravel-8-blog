<?php

namespace App\Http\Controllers;

use App\Services\Newsletter;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class NewsletterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  Request  $request
     * @param  Newsletter  $newsletter
     * @return RedirectResponse|Response
     */
    public function __invoke(Request $request, Newsletter $newsletter): Response|RedirectResponse
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withFragment('#newsletter')
                ->withErrors($validator->getMessageBag());
        }

        try {
            $newsletter->subscribe(request('email'));
        } catch (\Exception $e) {
            return redirect()
                ->back()
                ->withFragment('#newsletter')
                ->withInput()
                ->withErrors(
                    ['email' => 'Cannot add to the list. Either email is invalid or already in the newsletter.']
                );
        }

        return redirect()
            ->back()
            ->with('success', 'You are now signed up for our newsletter!');
    }
}
