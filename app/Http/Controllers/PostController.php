<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index(): View|Factory|Application
    {
        return view('posts.index', [
            'posts' => Post::latest('published_at')
                ->published()
                ->filter(request()->only(['search', 'category', 'author']))
                ->paginate(6)
                ->withQueryString(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create(): Application|Factory|View
    {
        return view('admin.posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StorePostRequest  $request
     * @return RedirectResponse
     */
    public function store(StorePostRequest $request): RedirectResponse
    {
        $attributes = $request->validated();

        $attributes['user_id'] = auth()->user()->id;

        if ($attributes['thumbnail'] ?? false) {
            $attributes['thumbnail'] = request()->file('thumbnail')->store('thumbnails');
        }

        $attributes['published_at'] = Carbon::now();

        $post = Post::create($attributes);

        $post->categories()->attach($attributes['categories']);

        return redirect("posts/{$post->slug}")->with([
            'success' => 'A new post has been published',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  Post  $post
     * @return Application|Factory|View
     */
    public function show(Post $post): View|Factory|Application
    {
        abort_if(! $post->isPublished(), 404);

        return view('posts.show', [
            'post' => $post->load([
                'comments' => function ($query) {
                    $query->orderByDesc('created_at');
                },
                'comments.author',
            ]),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdatePostRequest  $request
     * @param  Post  $post
     * @return RedirectResponse
     */
    public function update(UpdatePostRequest $request, Post $post): RedirectResponse
    {
        $attributes = $request->validated();

        if ($attributes['thumbnail'] ?? false) {
            Storage::delete($post->thumbnail);
            $attributes['thumbnail'] = request()->file('thumbnail')->store('thumbnails');
        }

        if (array_key_exists('is_published', $attributes)
            && $attributes['is_published'] == 1
            && ! $post->isPublished()) {
            //only publish the post if the post is unpublished
            $attributes['published_at'] = Carbon::now();
        } elseif (array_key_exists('is_published', $attributes) && $attributes['is_published'] == 0) {
            //remove the post from published status
            $attributes['published_at'] = null;
        }

        $post->update($attributes);

        if (array_key_exists('categories', $attributes)) {
            $post->categories()->sync($attributes['categories']);
        }

        return redirect()->back()->with('success', 'The post has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Post  $post
     * @return RedirectResponse
     *
     * @throws \Throwable
     */
    public function destroy(Post $post): RedirectResponse
    {
        $post->categories()->detach();
        $post->deleteOrFail();

        return redirect()
            ->back()
            ->with([
                'success' => 'Post has been deleted.',
            ]);
    }
}
