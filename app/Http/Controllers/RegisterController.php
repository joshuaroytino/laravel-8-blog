<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function create(): Factory|View|Application
    {
        return view('register.create');
    }

    public function store(Request $request)
    {
        $user = User::create(
            request()->validate([
                'name' => 'required|max:255',
                'username' => 'required|min:3|max:255|unique:users,username',
                'email' => 'required|email|max:255|unique:users,email',
                'password' => 'required|min:7',
            ])
        );

        $request->session()->regenerate();

        auth()->login($user);

        return redirect(route('home'))
            ->with('success', 'Your account has been created.');
    }
}
