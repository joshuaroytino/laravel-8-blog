<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Validation\ValidationException;

class SessionsController extends Controller
{
    public function create(): Factory|View|Application
    {
        return view('sessions.create');
    }

    public function authenticate(): Redirector|Application|RedirectResponse
    {
        $attributes = request()->validate([
            'email' => 'required|email|max:255',
            'password' => 'required|string',
        ]);

        if (! auth()->attempt($attributes)) {
            throw ValidationException::withMessages([
                'email' => 'Your provided credentials could not be verified.',
            ]);

            //or
            /*return back()
                ->withInput()
                ->withErrors(['email' => 'Your provided credentials could not be verified.']);*/
        }

        //fix for session fixation
        session()->regenerate();

        return redirect(route('home'))->with('success', 'Welcome Back!');
    }

    public function destroy(Request $request): Redirector|Application|RedirectResponse
    {
        auth()->logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect(route('home'))->with('success', 'Goodbye!');
    }
}
