<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class UpdatesController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  Request  $request
     * @return Application|Factory|View
     */
    public function __invoke(Request $request): Application|Factory|View
    {
        $updates = [
            '13-08-2022' => [
                'Updated the docker-compose file to use the .env for the database creation.',
                'Added steps in the README on how to setup the application.',
            ],
            '12-08-2022' => [
                'Make Quill editor a component.',
                'Make the required validation in the fields made with Quill editor work. Create method called isQuillEmpty() to check if the content of the Quill editor is empty.',
                'Add Quill editor to the body and excerpt fields for creating a post.',
                'Do not change the published_at value if the field is not empty. This is to prevent the published_at date to change when the post content is modified.',
                'Used @push and @stack to add scripts.',
                'Updated the server to Ubuntu 22.04 LTS',
            ],
            '11-08-2022' => [
                'Add Laravel Pint as Linter.',
                'Made a global XSS middleware, removed the auto formatting of texts with a paragraph.',
                'Added blade directive to check if user is an admin.',
                'Added ability to publish and unpublished a post.',
                'Unpublished post cannot be seen but can still be listed in the admin dashboard.',
                'Created a custom blade directive called @admin.',
            ],
            '05-08-2022' => [
                'Added WYSIWYG text editor(Quill) for edit post, also added link to view the post content.',
                'Added pagination to the admin dashboard of posts.',
                'Added HTMLPurifier to prevent XSS.',
                'Updated from Laravel 8 to Laravel 9.',
                'Updated files related to docker to use PHP 8.1.',
            ],
            '10-04-2022' => [
                'Add ability to like the posts.',
            ],
            '06-03-2022' => [
                'Added Unit/Feature Tests.',
                'Added SSL to the website using Let\s Encrypt.',
                'Added the ability to CRUD categories.',
            ],
            '01-03-2022' => [
                'Created a LAMP stack in a DigitalOcean droplet.',
                'Added CI/CD pipeline that is deployed to the created DigitalOcean droplet.',
                'Made the server hosted in DigitalOcean to have a domain.',
            ],
        ];

        return view('updates.index', compact('updates'));
    }
}
