<?php

namespace App\Http\Requests;

class UnlikeRequest extends LikeRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        //return $this->user()->can('unlike', $this->likeable());
        return true;
    }
}
