<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'sometimes|required|string|max:255',
            'thumbnail' => 'sometimes|nullable|image',
            'excerpt' => 'sometimes|required|string',
            'body' => 'sometimes|required|string',
            'categories' => 'sometimes|required|array|exists:categories,id',
            'categories.*' => 'required|integer',
            'is_published' => 'sometimes|required|integer|in:1,0',
        ];
    }
}
