<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Str;

class Category extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'slug'];

    protected static function booted()
    {
        parent::booted();

        static::created(function ($category) {
            $category->slug = Str::slug($category->name.' '.$category->id);
            $category->save();
        });

        static::updating(function ($category) {
            $category->slug = Str::slug($category->name.' '.$category->id);
        });
    }

    public function posts(): BelongsToMany
    {
        return $this->belongsToMany(Post::class, with(new CategoryPost)->getTable(), 'category_id', 'post_id');
    }
}
