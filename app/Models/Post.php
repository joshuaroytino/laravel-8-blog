<?php

namespace App\Models;

use App\Contracts\Likeable;
use App\Models\Concerns\Likes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Post extends Model implements Likeable
{
    use HasFactory, Likes;

    protected $fillable = [
        'title',
        'excerpt',
        'body',
        'user_id',
        'thumbnail',
        'published_at',
    ];

    protected $casts = [
        'published_at' => 'datetime',
    ];

    protected $with = [
        'categories',
        'author',
    ];

    protected static function booted()
    {
        parent::booted();

        static::created(function ($post) {
            $post->slug = Str::slug($post->title).'-'.$post->id;
            $post->save();
        });

        static::updating(function ($post) {
            $post->slug = Str::slug($post->title).'-'.$post->id;
        });
    }

    public function getThumbnailAttribute($thumbnail): string
    {
        return ! empty($thumbnail) ? "/storage/{$thumbnail}" : '/images/illustration-1.png';
    }

    /**
     * Scope a query to only include published posts.
     *
     * @param  Builder  $query
     * @return Builder
     */
    public function scopePublished(Builder $query): Builder
    {
        return $query->whereNotNull('published_at');
    }

    /**
     * Scope a query to only include posts based on search.
     *
     * @param  Builder  $query
     * @param  array  $filters
     * @return Builder
     */
    public function scopeFilter(Builder $query, array $filters): Builder
    {
        $query->when(
            $filters['search'] ?? false,
            fn ($query, $search) => $query->where(
                fn ($query) => $query->where('title', 'like', "%{$search}%")
                    ->orWhere('excerpt', 'like', "%{$search}%")
            )
        );

        $query->when(
            $filters['category'] ?? false,
            fn ($query, $category) => $query->whereHas('categories', fn ($query) => $query->where('slug', $category))
        );

        $query->when(
            $filters['author'] ?? false,
            fn ($query, $author) => $query->whereHas('author', fn ($query) => $query->where('username', $author))
        );

        return $query;
    }

    public function categories(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Category::class, with(new CategoryPost)->getTable(), 'post_id', 'category_id')
            ->withTimestamps();
    }

    public function author(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function comments(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Comment::class);
    }

    public function isPublished(): bool
    {
        return $this->published_at !== null;
    }
}
