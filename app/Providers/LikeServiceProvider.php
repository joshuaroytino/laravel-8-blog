<?php

namespace App\Providers;

use App\Contracts\Likeable;
use App\Models\User;
use Illuminate\Auth\Access\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class LikeServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Gate::define('like', function (User $user, Likeable $likeable) {
            if (! $likeable->exists) {
                return Response::deny("Cannot like an object that doesn't exists.");
            }

            if ($user->hasLiked($likeable)) {
                return Response::deny('Cannot like the same thing twice');
            }

            return Response::allow();
        });

        Gate::define('unlike', function (User $user, Likeable $likeable) {
            if (! $likeable->exists) {
                return Response::deny("Cannot like an object that doesn't exists.");
            }

            if (! $user->hasLiked($likeable)) {
                return Response::deny('Cannot unlike without like first.');
            }

            return Response::allow();
        });
    }
}
