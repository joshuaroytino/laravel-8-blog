<?php

namespace App\View\Components;

use App\Models\Category;
use Illuminate\View\Component;

class CategoryDropdown extends Component
{
    public function render(
    ): \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Support\Htmlable|\Closure|string|\Illuminate\Contracts\Foundation\Application {
        return view('components.category-dropdown', [
            'categories' => Category::all()->sortBy('name'),
            'currentCategory' => Category::firstWhere('slug', request('category')),
        ]);
    }
}
