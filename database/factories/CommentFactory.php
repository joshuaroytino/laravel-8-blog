<?php

namespace Database\Factories;

use App\Models\Post;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class CommentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $startDate = Carbon::now()->subYear(1);
        $endDate = Carbon::now();

        $createdAt = $this->faker->dateTimeBetween($startDate, $endDate);
        $updatedAt = $this->faker->dateTimeBetween($createdAt, $endDate);

        return [
            'body' => '<p>'.implode('</p><p>', $this->faker->paragraphs(6)).'</p>',
            'user_id' => User::factory(),
            'post_id' => Post::factory(),
            'created_at' => $createdAt,
            'updated_at' => $updatedAt,
        ];
    }
}
