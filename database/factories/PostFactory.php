<?php

namespace Database\Factories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $startDate = Carbon::now()->subYear(1);
        $endDate = Carbon::now();

        return [
            'slug' => $this->faker->unique->slug,
            'title' => $this->faker->sentence(3, 6),
            'excerpt' => '<p>'.implode('</p><p>', $this->faker->paragraphs(2)).'</p>',
            'body' => '<p>'.implode('</p><p>', $this->faker->paragraphs(6)).'</p>',
            'published_at' => $this->faker->dateTimeBetween($startDate, $endDate),
        ];
    }

    public function unPublished()
    {
        return $this->state(function (array $attributes) {
            return [
                'published_at' => null,
            ];
        });
    }
}
