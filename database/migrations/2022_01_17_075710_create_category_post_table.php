<?php

use App\Models\Category;
use App\Models\Post;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryPostTable extends Migration
{
    public function up()
    {
        Schema::create('categories_posts', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Category::class)
                ->constrained(with(new Category)->getTable());
            $table->foreignIdFor(Post::class)
                ->constrained(with(new Post)->getTable());
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('categories_posts');
    }
}
