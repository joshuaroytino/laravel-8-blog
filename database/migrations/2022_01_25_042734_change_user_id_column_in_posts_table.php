<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeUserIdColumnInPostsTable extends Migration
{
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropForeign(['user_id']);

            $table->foreignId('user_id')
                ->change() //should be after naming the column or errors will show when constraining the column
                ->constrained('users')
                ->cascadeOnDelete();
        });
    }

    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->foreignId('user_id')
                ->change()
                ->constrained('users');
        });
    }
}
