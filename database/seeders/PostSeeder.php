<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            $users = User::factory(10)->create();
            $categories = Category::factory(5)->create();
            $posts = Post::factory(250)->create(
                fn () => [
                    'user_id' => $users->random()->id,
                ]
            );

            $posts->each(function (Post $post) use ($categories, $users) {
                $post->categories()->attach($categories->random(rand(1, 3))->pluck('id')->toArray());
                Comment::factory(rand(1, 10))->create(
                    fn () => [
                        'user_id' => $users->random()->id,
                        'post_id' => $post->id,
                    ]
                );
            });
        });
    }
}
