<x-layout>
    <x-setting-page heading="Create New Category">
        <form method="POST" action="{{ route('admin.categories.store') }}" enctype="multipart/form-data">
            @csrf

            <x-form.input name='name' required/>

            <x-form.submit-button>Save</x-form.submit-button>
        </form>
    </x-setting-page>
</x-layout>
