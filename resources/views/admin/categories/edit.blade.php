<x-layout>
    <x-setting-page heading="Edit Category">
        <form method="POST" action="{{ route('admin.categories.update', $category) }}" enctype="multipart/form-data">
            @csrf
            @method('PATCH')

            <x-form.input name='name' value="{{ old('name', $category->name) }}" required/>

            <x-form.submit-button>Update</x-form.submit-button>
        </form>
    </x-setting-page>
</x-layout>
