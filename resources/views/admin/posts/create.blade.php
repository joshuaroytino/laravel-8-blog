<x-layout>
    <x-setting-page heading="Publish New Post">
        <form method="POST"
              action="{{ route('admin.posts.store') }}"
              enctype="multipart/form-data"
              x-data="data()"
              x-init="initQuill()" ,
              x-on:submit="submit()"
        >
            @csrf

            <x-form.input name='title' required/>
            <x-form.file name="thumbnail" required/>

            <div class="py-2">
                <x-form.quill name="excerpt"/>
            </div>

            <div class="py-2">
                <x-form.quill name="body"/>
            </div>

            <div class="mb-6">
                <label class="block mb-2 uppercase font-bold text-xs text-gray-700"
                       for="categories"
                >
                    Categories
                </label>
                <select class="border border-gray-400 p-2 w-full"
                        name="categories[]"
                        id="categories"
                        multiple
                >
                    @php
                        $categories = \App\Models\Category::all()->sortBy('name');
                    @endphp

                    @foreach ($categories as $category)
                        <option
                            value="{{ $category->id }}"
                            {{ old('categories') && in_array($category->id, old('categories')) ? 'selected' : '' }}>
                            {{ ucwords($category->name) }}
                        </option>
                    @endforeach
                </select>

                <x-form.error name="categories"/>
            </div>

            <x-form.submit-button>Publish</x-form.submit-button>
        </form>
    </x-setting-page>
    @push('scripts')
        <script>
            function data () {
                return {
                    initQuill () {
                        new Quill(this.$refs.quillExcerpt, quillOptions)
                        new Quill(this.$refs.quillBody, quillOptions)
                    },
                    submit () {
                        this.$refs.excerpt.value = this.$refs.quillExcerpt.__quill.root.innerHTML
                        this.$refs.body.value = this.$refs.quillBody.__quill.root.innerHTML

                        if (isQuillEmpty(this.$refs.quillExcerpt.__quill)) {
                            this.$refs.excerpt.value = ''
                        }

                        if (isQuillEmpty(this.$refs.quillBody.__quill)) {
                            this.$refs.body.value = ''
                        }
                    }
                }
            }
        </script>
    @endpush
</x-layout>
