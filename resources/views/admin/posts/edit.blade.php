<x-layout>
    <x-setting-page heading="Edit Post">
        <article class="text-right pt-1 pb-2">
            <a href="{{ route('post.show', $post->slug) }}"
               class="text-sm no-underline text-blue-600 hover:underline hover:text-blue-500"
               target="_blank">{{ route('post.show', $post->slug) }}</a>
        </article>
        <form method="POST"
              action="{{ url("admin/posts/{$post->id}") }}"
              enctype="multipart/form-data"
              x-data="data()"
              x-init="initQuill()"
              x-on:submit="submit()"
        >
            @csrf
            @method('PATCH')

            <x-form.input name='title' value="{{ old('title', $post->title) }}"/>
            <div class="flex mt-6">
                <div class="flex-auto w-10/12">
                    <x-form.file name="thumbnail"/>
                </div>

                <img src="{{ asset($post->thumbnail) }}"
                     alt="{{ $post->title }} thumbnail"
                     class="rounded-xl ml-4 flex-auto w-2/12"/>
            </div>

            <div class="py-2">
                <x-form.quill name="excerpt">{!! $post->excerpt !!}</x-form.quill>
            </div>

            <div class="py-2">
                <x-form.quill name="body">{!! $post->body !!}</x-form.quill>
            </div>

            <div>
                <label class="block mb-2 uppercase font-bold text-xs text-gray-700"
                       for="categories"
                >
                    Categories
                </label>
                <select class="border border-gray-400 p-2 w-full"
                        name="categories[]"
                        id="categories"
                        multiple
                >
                    @php
                        $categories = \App\Models\Category::all()->sortBy('name');
                    @endphp

                    @foreach ($categories as $category)
                        <option
                            value="{{ $category->id }}"
                            {{ old('categories', $post->categories->pluck('id')->toArray()) && in_array($category->id, old('categories', $post->categories->pluck('id')->toArray())) ? 'selected' : '' }}>
                            {{ ucwords($category->name) }}
                        </option>
                    @endforeach
                </select>

                <x-form.error name="categories"/>
            </div>

            <div class="mb-6">
                <x-form.toggle name="is_published" toggle="{{ $post->isPublished() ? 1 : 0 }}"/>
            </div>

            <x-form.submit-button>Update</x-form.submit-button>
        </form>
    </x-setting-page>

    @push('scripts')
        <script>
            function data () {
                return {
                    initQuill () {
                        new Quill(this.$refs.quillExcerpt, quillOptions)
                        new Quill(this.$refs.quillBody, quillOptions)
                    },
                    submit () {
                        this.$refs.excerpt.value = this.$refs.quillExcerpt.__quill.root.innerHTML
                        this.$refs.body.value = this.$refs.quillBody.__quill.root.innerHTML

                        if (isQuillEmpty(this.$refs.quillExcerpt.__quill)) {
                            this.$refs.excerpt.value = ''
                        }

                        if (isQuillEmpty(this.$refs.quillBody.__quill)) {
                            this.$refs.body.value = ''
                        }
                    }
                }
            }
        </script>
    @endpush
</x-layout>
