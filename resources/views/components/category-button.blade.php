@props(['category'])

<a href="/?{{ http_build_query(
        collect(['category' => $category->slug])
            ->merge(request()->except(['page', 'category']))
            ->all()
    ) }}"
   class="transition-colors duration-300 px-3 py-1 border border-blue-300 rounded-full text-blue-300 text-xs uppercase font-semibold hover:bg-blue-600 hover:text-white"
   style="font-size: 10px">{{ $category->name }}</a>
