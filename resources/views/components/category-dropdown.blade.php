<x-dropdown>
    <x-slot name="trigger">
        <button
            class="flex-1 appearance-none bg-transparent py-2 pl-3 pr-9 text-sm font-semibold w-full text-left lg:inline-flex flex">
            {{ isset($currentCategory) ? ucwords($currentCategory->name) : 'Categories' }}
            <x-icon
                name="down-arrow"
                class="absolute pointer-events-none"
            />
        </button>
    </x-slot>

    <x-dropdown-item href="/?{{ http_build_query( request()->except('category')) }}" :active='!$currentCategory'>All
    </x-dropdown-item>

    @foreach ($categories as $category)
        <x-dropdown-item
            href="/?{{ http_build_query(
                                collect(['category' => $category->slug])
                                    ->merge(request()->except(['category', 'page']))
                                    ->all()
                            ) }}"
            :active='$category->is($currentCategory)'
        >
            {{ ucfirst($category->name) }}
        </x-dropdown-item>
    @endforeach
</x-dropdown>
