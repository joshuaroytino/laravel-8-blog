@props(['name'])
@php
    // You need to activate this in a script in order for this component to work
    // new Quill(this.$refs.name, quillOptions)
@endphp
<x-form.label name="{{ $name }}"/>
<div x-ref="quill{{ ucfirst($name) }}" class="py-2">{!! old($name) ?? $slot !!}</div>
<x-form.error name="{{ $name }}"/>
<x-form.textarea name="{{ $name }}" class="hidden"/>
