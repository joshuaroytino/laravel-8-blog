@props(['name', 'class'])
@php
    $containerClass = 'mb-6';
    if($class ?? false) $containerClass .= ' ' . $class;
@endphp
<div {{ $attributes(['class' => $containerClass]) }}>
    <x-form.label name="{{ $name }}"/>

    <textarea class="border border-gray-400 p-2 w-full rounded"
              name="{{ $name }}"
              id="{{ $name }}"
              x-ref="{{ $name }}"
    >{{ old($name) ?? $slot }}</textarea>

    <x-form.error name="{{ $name }}"/>
</div>
