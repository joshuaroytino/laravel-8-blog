@props(['name', 'toggle' => 0, 'on' => 'Published',  'off' => 'Unpublished'])
<div class="flex mt-8" x-data="{ toggle: {{ $toggle }} }">
    <div
        class="relative w-12 h-6 transition duration-200 ease-linear rounded-full"
        :class="[toggle ? 'bg-green-400' : 'bg-gray-400']"
    >
        <label
            for="{{$name}}"
            class="absolute left-0 w-6 h-6 mb-2 transition duration-100 ease-linear transform bg-white border-2 rounded-full cursor-pointer"
            :class="[toggle === 1 ? 'translate-x-full border-green-400' : 'translate-x-0 border-gray-400']"
        ></label>
        <input
            type="checkbox"
            id="{{$name}}"
            name="{{$name}}"
            class="w-full h-full appearance-none focus:outline-none"
            @click="toggle ? toggle = 0 : toggle = 1"
            :value="toggle"
        />
    </div>
    <span x-show="toggle === 1" class="ml-1">{{$on}}</span>
    <span x-show="toggle === 0" class="ml-1">{{$off}}</span>
</div>
