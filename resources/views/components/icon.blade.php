@props(['name'])

@if ($name === 'down-arrow')
    <x-down-arrow :attributes="$attributes"/>
@endif
