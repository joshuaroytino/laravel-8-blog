<!doctype html>
<html lang="en">
<meta name="csrf-token" content="{{ csrf_token() }}"/>
<title>Laravel From Scratch Blog</title>
<link href="{{ mix('css/app.css') }}" rel="stylesheet">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap" rel="stylesheet">
<link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">

<style>
    html {
        scroll-behavior: smooth;
    }

    .clamp {
        display: -webkit-box;
        -webkit-box-orient: vertical;
        overflow: hidden;
    }

    .clamp.one-line {
        -webkit-line-clamp: 1;
    }

    .ql-editor {
        height: 12.5em;
        min-height: 12.5em;
        max-height: 12.5em;
    }

    .ql-editor p {
        margin-bottom: 1rem;
    }

    [x-cloak] {
        display: none;
    }
</style>

<body style="font-family: Open Sans, sans-serif">
<section class="px-6 py-8">
    <nav class="md:flex md:justify-between md:items-center">
        <div class="font-extrabold text-3xl text-gray-700">
            <a href="/">
                DevJT
            </a>
        </div>

        <div class="mt-8 md:mt-0 flex items-baseline gap-8">
            <a href="{{ route('updates') }}"
               class="transition-colors duration-300 text-xs font-bold uppercase hover:text-blue-500 {{ Route::is('updates') ? 'border-b-2 border-b-blue-500' : '' }}">Updates</a>

            @guest
                <a href="{{ route('register.create') }}"
                   class="transition-colors duration-300 text-xs font-bold uppercase hover:text-blue-500 {{ Route::is('register.create') ? 'border-b-2 border-b-blue-500' : '' }}">Register</a>
            @endguest

            @guest
                <a href="{{ route('login.create') }}"
                   class="transition-colors duration-300 text-xs font-bold uppercase hover:text-blue-500 {{ Route::is('login.create') ? 'border-b-2 border-b-blue-500' : '' }}">Log
                    In</a>
            @endguest

            @auth
                <x-dropdown>
                    <x-slot name="trigger">
                        <button class="transition-colors duration-300 text-xs font-bold uppercase hover:text-blue-500">
                            Welcome, {{ auth()->user()->name }}</button>
                    </x-slot>

                    @isAdmin()
                    <x-dropdown-item href="{{ route('admin.posts.index') }}"
                                     active="{{ request()->routeIs('admin.posts.index') }}">Dashboard
                    </x-dropdown-item>
                    @endIsAdmin

                    @isAdmin()
                    <x-dropdown-item href="{{ route('admin.posts.create') }}"
                                     active="{{ request()->routeIs('admin.posts.create') }}">New Post
                    </x-dropdown-item>
                    @endIsAdmin

                    <x-dropdown-item href="#" x-data="{}"
                                     @click.prevent="document.querySelector('form#logout').submit()">Log Out
                    </x-dropdown-item>

                    <form id="logout" method="POST" action="{{ route('logout') }}"
                          class="text-xs text-blue-500 ml-6">
                        @csrf
                    </form>
                </x-dropdown>
            @endauth

            <a href="#newsletter"
               class="bg-blue-500 ml-3 rounded-full text-xs font-semibold text-white uppercase py-3 px-5 hidden">
                Subscribe for Updates
            </a>
        </div>
    </nav>

    {{ $slot }}

    <footer id="newsletter"
            class="bg-gray-100 border border-black border-opacity-5 rounded-xl text-center py-16 px-10 mt-16 hidden">
        <img src="{{ url('images/lary-newsletter-icon.svg') }}" alt="" class="mx-auto -mb-6" style="width: 145px;">
        <h5 class="text-3xl">Stay in touch with the latest posts</h5>
        <p class="text-sm mt-3">Promise to keep the inbox clean. No bugs.</p>

        <div class="mt-10">
            <div class="relative inline-block mx-auto lg:bg-gray-200 rounded-full">

                <form method="POST" action="{{ route('newsletter') }}" class="lg:flex text-sm">
                    @csrf
                    <div class="lg:py-3 lg:px-5 flex items-center">
                        <label for="email" class="hidden lg:inline-block">
                            <img src="{{ url('images/mailbox-icon.svg') }}" alt="mailbox letter">
                        </label>

                        <div>
                            <input id="email" name="email" type="text" placeholder="Your email address"
                                   class="lg:bg-transparent py-2 lg:py-0 pl-4 focus-within:outline-none">
                        </div>
                    </div>

                    <button type="submit"
                            class="transition-colors duration-300 bg-blue-500 hover:bg-blue-600 mt-4 lg:mt-0 lg:ml-3 rounded-full text-xs font-semibold text-white uppercase py-3 px-8"
                    >
                        Subscribe
                    </button>
                </form>
            </div>

            <x-form.error name="email"/>

        </div>
    </footer>
</section>

@if (session()->has('success'))
    <div x-data="{ show: true }"
         x-init="setTimeout(() => show = false, 4000)"
         x-show="show"
         class="fixed bg-green-500 text-white py-2 px-4 rounded-xl bottom-3 right-3 text-sm"
    >
        <p>{{ session('success') }}</p>
    </div>
@endif

@if (session()->has('error'))
    <div x-data="{ show: true }"
         x-init="setTimeout(() => show = false, 4000)"
         x-show="show"
         class="fixed bg-red-500 text-white py-2 px-4 rounded-xl bottom-3 right-3 text-sm"
    >
        <p>{{ session('error') }}</p>
    </div>
@endif

<script src="//unpkg.com/alpinejs" defer></script>
<script src="//cdn.quilljs.com/1.3.6/quill.min.js"></script>
<script>
    const quillOptions = {
        theme: 'snow',
        modules: {
            clipboard: {
                matchVisual: false
            }
        }
    }

    const isQuillEmpty = quill => {
        return quill.getText().trim().length === 0 && quill.container.firstChild.innerHTML.includes('img') === false
    }
</script>
@stack('scripts')
</body>
</html>
