@props(['post', 'iteration'])

<article
    {{ $attributes->merge(['class' => 'transition-colors duration-300 hover:bg-gray-100 border border-black border-opacity-0 hover:border-opacity-5 rounded-xl']) }}>
    <div class="py-6 px-5 min-h-full flex flex-col">
        <div>
            <img src="{{ asset($post->thumbnail) }}" alt="{{ $post->title }} Thumbnail" class="rounded-xl">
        </div>

        <div class="mt-8 grow flex flex-col">
            <header>
                <div class="space-x-2">
                    @foreach($post->categories as $category)
                        <x-category-button :category="$category"/>
                    @endforeach
                </div>

                <div class="mt-4">
                    <h1 class="text-3xl">
                        <a href="/posts/{{ $post->slug }}">{{ $post->title }}</a>
                    </h1>

                    <span class="mt-2 block text-gray-400 text-xs">
                        Published <time>{{ $post->published_at->diffForHumans() }}</time>
                    </span>
                </div>
            </header>

            <div class="text-sm mt-4 space-y-4 mb-auto pb-4">
                {!! $post->excerpt !!}
            </div>

            <footer @class([
                "flex justify-between items-center mt-auto pt-4",
                "lg:flex-col lg:space-y-4" => $iteration >= 3,
            ])>
                <div class="flex items-center text-sm">
                    <img src="https://i.pravatar.cc/60?u={{ $post->author->id }}" alt="Lary avatar" class="rounded-2xl">
                    <div class="ml-3 line-clamp-1">
                        <h5 class="font-bold">
                            <a href="/?{{ http_build_query(
                                collect(['author' => $post->author->username])
                                    ->merge(request()->except('author'))
                                    ->all()
                            ) }}">{{ $post->author->name }}</a>
                        </h5>
                    </div>
                </div>

                <div>
                    <a href="/posts/{{ $post->slug }}"
                       class="transition-colors duration-300 text-xs font-semibold bg-gray-200 hover:bg-gray-300 rounded-full py-2 px-8"
                    >Read More</a>
                </div>
            </footer>
        </div>
    </div>
</article>
