@props(['heading'])
<section class="py-8 max-w-4xl mx-auto">
    <h1 class="text-lg font-bold mb-8 pb-2 border-b">
        {{ $heading }}
    </h1>

    <div class="flex">
        <aside class="w-48 flex-shrink-0">
            <h4 class="font-semibold mb-4">Links</h4>

            <ul>
                <li>
                    <a href="{{ route('admin.posts.index') }}"
                       class="block text-left px-3 text-sm leading-6 hover:bg-blue-500 focus:bg-blue-500 hover:text-white focus:text-white {{ request()->routeIs('admin.posts.index') ? 'bg-blue-500 text-white' : '' }}">
                        All Posts
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.posts.create') }}"
                       class="block text-left px-3 text-sm leading-6 hover:bg-blue-500 focus:bg-blue-500 hover:text-white focus:text-white {{ request()->routeIs('admin.posts.create') ? 'bg-blue-500 text-white' : '' }}">
                        New Post
                    </a>
                </li>
                <hr class="border-4 border-dark"/>
                <li>
                    <a href="{{ route('admin.categories.index') }}"
                       class="block text-left px-3 text-sm leading-6 hover:bg-blue-500 focus:bg-blue-500 hover:text-white focus:text-white {{ request()->routeIs('admin.categories.index') ? 'bg-blue-500 text-white' : '' }}">
                        All Categories
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.categories.create') }}"
                       class="block text-left px-3 text-sm leading-6 hover:bg-blue-500 focus:bg-blue-500 hover:text-white focus:text-white {{ request()->routeIs('admin.categories.create') ? 'bg-blue-500 text-white' : '' }}">
                        New Category
                    </a>
                </li>
            </ul>
        </aside>

        <main class="flex-1">
            <x-panel>
                {{ $slot }}
            </x-panel>
        </main>
    </div>
</section>
