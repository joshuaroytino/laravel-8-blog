@auth
    @php
        $liked = Auth::user()?->hasLiked($model);
    @endphp
    <p class="hidden">Alpine with AJAX</p>
    <div class="flex flex-row">
        <form
            action="{{ route('like') }}"
            method="POST"
            x-data="like()"
            @showlike.window="isShown = true; isDisabled = false"
            @submit.prevent="submitData()"
        >
            @csrf
            <input type="hidden" name="likeable_type" x-model="formData.likeable_type"/>
            <input type="hidden" name="id" x-model="formData.likeable_id"/>
            <button
                :class="{'hidden': !isShown, 'cursor-not-allowed ': isDisabled}"
                type="submit"
                x-cloak
                x-bind:disabled="isDisabled"
            >
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24"
                     stroke="currentColor" stroke-width="2" title="Like">
                    <path stroke-linecap="round" stroke-linejoin="round"
                          d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z"/>
                </svg>
            </button>
        </form>

        <form
            action="{{ route('unlike') }}"
            method="POST"
            x-data="unlike()"
            @showunlike.window="isShown = true; isDisabled = false"
            @submit.prevent="submitData()"
        >
            @csrf
            @method('DELETE')
            <input type="hidden" name="likeable_type" x-model="formData.likeable_type"/>
            <input type="hidden" name="id" x-model="formData.likeable_id"/>
            <button
                :class="{'hidden': !isShown, 'cursor-not-allowed': isDisabled}"
                type="submit"
                x-cloak
                x-bind:disabled="isDisabled"
            >
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="red" viewBox="0 0 24 24"
                     stroke="currentColor" stroke-width="2" title="Like">
                    <path stroke-linecap="round" stroke-linejoin="round"
                          d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z"/>
                </svg>
            </button>
        </form>
    </div>

    <script type="text/javascript">
        function like () {
            return {
                formData: {
                    likeable_type: '{{ addslashes(get_class($model)) }}',
                    likeable_id: {{ $model->id }}
                },
                message: '',
                isShown: {{ var_export(!$liked) }},
                isDisabled: false,
                submitData () {
                    this.message = ''
                    this.isDisabled = true

                    fetch('{{ route('like') }}', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            'X-CSRF-TOKEN': document.head.querySelector('meta[name=csrf-token]').content,
                            'X-Requested-With': 'XMLHttpRequest'
                        },
                        body: JSON.stringify(this.formData)
                    })
                        .then(() => {
                            this.message = 'Added to likes'
                            this.isShown = !this.isShown
                            window.dispatchEvent(new CustomEvent('showunlike'))
                        })
                        .catch(() => this.message = 'Cannot add to likes')

                }
            }
        }

        function unlike () {
            return {
                formData: {
                    likeable_type: '{{ addslashes(get_class($model)) }}',
                    likeable_id: {{ $model->id }},
                    _method: 'DELETE'
                },
                message: '',
                isShown: {{ var_export($liked) }},
                isDisabled: false,
                submitData () {
                    this.message = ''
                    this.isDisabled = true

                    fetch('{{ route('unlike') }}', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            'X-CSRF-TOKEN': document.head.querySelector('meta[name=csrf-token]').content,
                            'X-Requested-With': 'XMLHttpRequest'
                        },
                        body: JSON.stringify(this.formData)
                    })
                        .then(() => {
                            this.message = 'Removed from likes'
                            this.isShown = !this.isShown
                            window.dispatchEvent(new CustomEvent('showlike'))
                        })
                        .catch(() => this.message = 'Cannot remove from likes')

                }
            }
        }

    </script>
@endauth
