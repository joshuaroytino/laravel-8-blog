@auth
    <p class="hidden">Blade-only</p>
    @can('like', $model)
        <form action="{{ route('like') }}" method="POST">
            @csrf
            <input type="hidden" name="likeable_type" value="{{ get_class($model) }}"/>
            <input type="hidden" name="likeable_id" value="{{ $model->id }}"/>
            <button class="hidden">@lang('Like')</button>
        </form>
    @endcan

    @can('unlike', $model)
        <form action="{{ route('unlike') }}" method="POST">
            @csrf
            @method('DELETE')
            <input type="hidden" name="likeable_type" value="{{ get_class($model) }}"/>
            <input type="hidden" name="likeable_id" value="{{ $model->id }}"/>
            <button class="hidden">@lang('Unlike')</button>
        </form>
    @endcan
@endauth
