@auth
    <x-panel>
        <form method="POST" action="/posts/{{ $post->slug }}/comments">
            @csrf
            <header class="flex items-center">
                <img src="https://i.pravatar.cc/60?u={{ auth()->id() }}" alt="" width="40"
                     class="rounded-full"/>
                <h2 class="ml-4">Want to Participate?</h2>
            </header>

            <div class="mt-6">
                <textarea name="body"
                          class="w-full text-sm px-2 py-1 {{ $errors->first('body', 'border-red-500') }}"
                          rows="5"
                          placeholder="Quick, think of something to say"></textarea>
                <x-form.error name="body"/>
            </div>

            <x-form.submit-button>Submit</x-form.submit-button>
        </form>
    </x-panel>
@else
    <p class="font-semibold">
        <a href="{{ route('register.create') }}"
           class="text-blue-400 hover:underline hover:text-blue-500">Register</a>
        or <a href="{{ route('login.create') }}"
              class="text-blue-400 hover:underline hover:text-blue-500">log in</a> to leave a
        comment.
    </p>
@endauth
