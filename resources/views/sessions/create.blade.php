<x-layout>
    <section class="px-6 py-8">
        <main class="max-w-lg mx-auto mt-10">
            <x-panel>
                <h1 class="text-center font-bold text-xl">Log In</h1>
                <form class="mt-10" method="POST" action="{{ route('login.authenticate') }}">
                    @csrf
                    <x-form.input name="email" type="email" autocomplete="username" required/>
                    <x-form.input name="password" type="password" autocomplete="current-password" required/>
                    <x-form.submit-button>Log In</x-form.submit-button>
                </form>
            </x-panel>
        </main>
    </section>
</x-layout>
