<x-layout>
    <div class="max-w-xl mx-auto mt-20">
        @foreach ($updates as $date => $activities)
            <span class="text-lg font-weight-bolder text-left">
                <h3>{{ date('F j, Y', strtotime($date)) }}</h3>
                <ul class="list-disc text-left mb-5 ml-3">
                    @foreach ($activities as $activity)
                        <li>{{ $activity }}</li>
                    @endforeach
                </ul>
            </span>
        @endforeach
    </div>
</x-layout>
