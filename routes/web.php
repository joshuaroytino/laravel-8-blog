<?php

use App\Http\Controllers\AdminCategoryController;
use App\Http\Controllers\AdminPostController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\LikeController;
use App\Http\Controllers\NewsletterController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\SessionsController;
use App\Http\Controllers\UpdatesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function () {
    Route::prefix('admin')
        ->middleware('admin')
        ->as('admin.')
        ->group(function () {
            Route::controller(PostController::class)
                ->prefix('/posts')
                ->as('posts.')
                ->group(function () {
                    Route::get('create', 'create')->name('create');
                    Route::post('', 'store')->name('store');
                    Route::patch('{post}', 'update')->name('update');
                    Route::delete('{post}', 'destroy')->name('destroy');
                });

            Route::controller(AdminPostController::class)
                ->prefix('/posts')
                ->as('posts.')
                ->group(function () {
                    Route::get('', 'index')->name('index');
                    Route::get('{post}/edit', 'edit')->name('edit');
                });

            Route::controller(AdminCategoryController::class)
                ->prefix('/categories')
                ->as('categories.')
                ->group(function () {
                    Route::get('', 'index')->name('index');
                    Route::get('create', 'create')->name('create');
                    Route::post('', 'store')->name('store');
                    Route::get('edit/{category:slug}', 'edit')->name('edit');
                    Route::patch('category/{category:slug}', 'update')->name('update');
                    Route::delete('category/{category:slug}', 'destroy')->name('destroy');
                });
        });

    Route::post('posts/{post:slug}/comments', [CommentController::class, 'store'])
        ->name('posts.comments');

    Route::post('like', [LikeController::class, 'like'])->name('like');
    Route::delete('like', [LikeController::class, 'unlike'])->name('unlike');

    Route::post('logout', [SessionsController::class, 'destroy'])
        ->name('logout');
});

Route::group(['middleware' => 'guest'], function () {
    Route::get('register', [RegisterController::class, 'create'])
        ->name('register.create');

    Route::post('register', [RegisterController::class, 'store'])
        ->name('register.store');

    Route::get('login', [SessionsController::class, 'create'])
        ->name('login.create');

    Route::post('login', [SessionsController::class, 'authenticate'])
        ->name('login.authenticate');
});

Route::get('/', [PostController::class, 'index'])->name('home');
Route::get('posts/{post:slug}', [PostController::class, 'show'])
    ->name('post.show');

Route::post('newsletter', NewsletterController::class)
    ->name('newsletter');

Route::get('updates', UpdatesController::class)->name('updates');
