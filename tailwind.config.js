module.exports = {
  content: [
    './resources/**/*.blade.php',
    './resources/**/*.js',
    './resources/**/*.vue',
  ],
  theme: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/line-clamp'),
  ],
  /**
   * Added configuration to add all the Tailwind classes.
   * Since v3 is JIT
   */
  /*safelist: [
    {
      pattern: /./,
    }
  ]*/
}
