<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AdminHeaderTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function setUp(): void
    {
        parent::setUp();

        $nonAdminUser = User::factory(1)
            ->isAdmin()
            ->createOne();
        $this->actingAs($nonAdminUser);
    }

    public function testCanViewDashboardLink()
    {
        $this->get('/')
            ->assertSee('Dashboard');
    }

    public function testCanViewCreatePostLink()
    {
        $this->get('/')
            ->assertSee('New Post');
    }

    public function testCanViewLogoutLink()
    {
        $this->get('/')
            ->assertSee('Log Out');
    }
}
