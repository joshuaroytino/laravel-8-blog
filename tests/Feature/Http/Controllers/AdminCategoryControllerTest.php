<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AdminCategoryControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_authorized_user_can_go_to_the_list_of_categories_page()
    {
        $this->actingAsAuthorizedUser();

        $this->get(route('admin.categories.index'))
            ->assertOk()
            ->assertViewIs('admin.categories.index');
    }

    public function test_authorized_user_can_go_to_the_create_category_page()
    {
        $this->actingAsAuthorizedUser();

        $this->get(route('admin.categories.create'))
            ->assertOk()
            ->assertViewIs('admin.categories.create');
    }

    public function test_authorized_user_can_save_category_successfully()
    {
        $this->actingAsAuthorizedUser();

        $data = ['name' => 'a valid category'];

        $this->post(route('admin.categories.store'), $data)
            ->assertRedirect()
            ->assertSessionHasNoErrors();

        $this->assertDatabaseHas(Category::class, $data);
    }

    /**
     * @dataProvider validation_provider
     */
    public function test_authorized_user_save_category_with_validation_errors($getData)
    {
        $this->actingAsAuthorizedUser();

        [$fields, $payload] = $getData();

        $this->post(route('admin.categories.store'), $payload)
            ->assertRedirect()
            ->assertSessionHasErrors($fields);
    }

    public function test_authorized_user_can_visit_the_category_edit_page()
    {
        $this->actingAsAuthorizedUser();

        $category = Category::factory()->createOne();

        $this->get(route('admin.categories.edit', $category))
            ->assertOk();
    }

    public function test_authorized_user_can_update_the_category_successfully()
    {
        $this->actingAsAuthorizedUser();

        $oldName = 'old-category';
        $category = Category::factory()->createOne([
            'name' => $oldName,
        ]);

        $newCategoryData = [
            'name' => 'new-category',
        ];

        $this->patch(route('admin.categories.update', $category), $newCategoryData)
            ->assertRedirect()
            ->assertSessionHasNoErrors();

        $category->refresh();

        $this->assertDatabaseHas(Category::class, ['id' => $category->id]);

        $this->assertFalse($category->name === $oldName);
        $this->assertTrue($category->name === $newCategoryData['name']);
    }

    /**
     * @dataProvider validation_provider
     */
    public function test_authorized_user_update_category_with_validation_errors($getData)
    {
        $this->actingAsAuthorizedUser();

        $category = Category::factory()->createOne();

        [$fields, $payload] = $getData();

        $this->patch(route('admin.categories.update', $category), $payload)
            ->assertRedirect()
            ->assertSessionHasErrors($fields);
    }

    public function test_authorized_user_can_update_category_with_same_name()
    {
        $this->actingAsAuthorizedUser();

        $oldName = 'same-category';
        $category = Category::factory()->createOne([
            'name' => $oldName,
        ]);
        $newCategoryData = [
            'name' => 'same-category',
        ];

        $this->patch(route('admin.categories.update', $category), $newCategoryData)
            ->assertRedirect()
            ->assertSessionHasNoErrors();

        $category->refresh();

        $this->assertDatabaseHas(Category::class, ['id' => $category->id]);

        $this->assertTrue($category->name === $oldName);
        $this->assertTrue($category->name === $newCategoryData['name']);
    }

    public function test_authorized_user_can_delete_a_category_without_post()
    {
        $this->actingAsAuthorizedUser();

        $category = Category::factory()->createOne();

        $this->delete(route('admin.categories.destroy', $category))
            ->assertRedirect()
            ->assertSessionHasNoErrors();

        $this->assertDatabaseMissing(Category::class, ['id' => $category->id]);
    }

    public function test_authorized_user_cannot_delete_a_category_with_post()
    {
        $user = $this->actingAsAuthorizedUser();
        $category = Category::factory()->createOne();
        $post = Post::factory()->createOne([
            'user_Id' => $user->id,
        ]);
        $post->categories()->attach([$category->id]);

        $this->delete(route('admin.categories.destroy', $category))
            ->assertRedirect()
            ->assertSessionHas(['error']);

        $this->assertDatabaseHas(Category::class, ['id' => $category->id]);
    }

    public function test_unauthorized_user_cannot_go_to_the_list_of_categories_page()
    {
        $this->actingAsUnauthorizedUser();

        $this->get(route('admin.categories.index'))
            ->assertForbidden();
    }

    public function test_unauthorized_user_cannot_go_to_the_create_category_page()
    {
        $this->actingAsUnauthorizedUser();

        $this->get(route('admin.categories.create'))
            ->assertForbidden();
    }

    public function test_unauthorized_user_cannot_save_a_category()
    {
        $this->actingAsUnauthorizedUser();

        $this->post(route('admin.categories.store'))
            ->assertForbidden();
    }

    public function test_unauthorized_user_cannot_visit_category_edit_page()
    {
        $this->actingAsUnauthorizedUser();

        $category = Category::factory()->createOne();

        $this->get(route('admin.categories.edit', $category))
            ->assertForbidden();
    }

    public function test_unauthorized_user_cannot_delete_a_category()
    {
        $this->actingAsUnauthorizedUser();
        $category = Category::factory()->createOne();

        $this->delete(route('admin.categories.destroy', $category))
            ->assertForbidden();
    }

    public function test_unauthenticated_user_cannot_go_to_the_list_of_categories_page()
    {
        $this->get(route('admin.categories.index'))
            ->assertRedirect(route('login.authenticate'));
    }

    public function test_unauthenticated_user_cannot_go_to_the_create_category_page()
    {
        $this->get(route('admin.categories.create'))
            ->assertRedirect(route('login.authenticate'));
    }

    public function test_unauthenticated_user_cannot_save_a_category()
    {
        $this->post(route('admin.categories.store'))
            ->assertRedirect(route('login.authenticate'));
    }

    public function test_unauthenticated_user_cannot_visit_category_edit_page()
    {
        $category = Category::factory()->create();

        $this->get(route('admin.categories.edit', $category))
            ->assertRedirect(route('login.authenticate'));
    }

    public function test_unauthenticated_user_cannot_delete_a_category()
    {
        $category = Category::factory()->create();

        $this->delete(route('admin.categories.destroy', $category))
            ->assertRedirect(route('login.authenticate'));
    }

    private function validation_provider(): array
    {
        return [
            'it fails if payload is empty' => [
                function () {
                    return [
                        ['name'],
                        [],
                    ];
                },
            ],
            'it fails if name is empty string' => [
                function () {
                    return [
                        ['name'],
                        ['name' => ''],
                    ];
                },
            ],
            'it fails if name is above 255 characters' => [
                function () {
                    return [
                        ['name'],
                        ['name' => str_repeat('a', 256)],
                    ];
                },
            ],
            'it fails if category name is not unique' => [
                function () {
                    Category::factory()->createOne(['name' => 'already-exists']);

                    return [
                        ['name'],
                        ['name' => 'already-exists'],
                    ];
                },
            ],
        ];
    }
}
