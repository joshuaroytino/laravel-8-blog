<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CommentControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_authenticated_user_can_comment()
    {
        $author = User::factory()->createOne();
        $post = Post::factory()->createOne([
            'user_id' => $author->id,
        ]);
        $commenter = User::factory()->createOne();

        $this->actingAs($commenter);

        $this->post(route('posts.comments', $post), [
            'body' => 'This is my comment',
        ])
            ->assertRedirect()
            ->assertSessionHasNoErrors();
    }

    /**
     * @dataProvider validation_provider
     */
    public function test_comment_fails_due_to_validation_errors($getData)
    {
        [$fields, $payload] = $getData();

        $author = User::factory()->create();
        $post = Post::factory()->create([
            'user_id' => $author->id,
        ]);

        $commenter = User::factory()->create();

        $this->actingAs($commenter);

        $this->post(route('posts.comments', $post), $payload)
            ->assertSessionHasErrors($fields);
    }

    public function test_unauthenticated_user_cannot_comment()
    {
        $author = User::factory()->createOne();
        $post = Post::factory()->createOne([
            'user_id' => $author->id,
        ]);

        $this->post(route('posts.comments', $post))
            ->assertRedirect(route('login.authenticate'));
    }

    private function validation_provider()
    {
        return [
            'it fails if payload is empty' => [
                function () {
                    return [
                        ['body'],
                        [],
                    ];
                },
            ],
            'it fails if body is empty string' => [
                function () {
                    return [
                        ['body'],
                        ['body' => ''],
                    ];
                },
            ],
            'it fails if body is an array' => [
                function () {
                    return [
                        ['body'],
                        ['body' => []],
                    ];
                },
            ],
        ];
    }
}
