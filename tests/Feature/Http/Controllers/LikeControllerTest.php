<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\Like;
use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class LikeControllerTest extends TestCase
{
    use RefreshDatabase;

    private User $author;

    private Post $postRecord;

    private User $anotherUser;

    public function setUp(): void
    {
        parent::setUp();

        $this->author = User::factory()->createOne();
        $this->postRecord = Post::factory()->createOne([
            'user_id' => $this->author->id,
        ]);

        $this->anotherUser = User::factory()->createOne();
    }

    public function test_authenticated_user_can_like_post()
    {
        $this->actingAs($this->anotherUser);

        $this->post(route('like'), $this->likeableInput())
            ->assertRedirect()
            ->assertSessionHasNoErrors();

        $this->assertDatabaseHas('likes', array_merge($this->likeableInput(), ['user_id' => $this->anotherUser->id]));
    }

    /**
     * @dataProvider validation_provider
     */
    public function test_authenticated_user_like_post_with_validation_errors($getData)
    {
        [$fields, $payload] = $getData();

        $this->actingAs($this->anotherUser);

        $this->post(route('like'), $payload)
            ->assertRedirect()
            ->assertSessionHasErrors($fields);
    }

    public function test_authenticated_user_can_unlike_post()
    {
        $user = User::factory()->createOne();
        Like::query()->insert(array_merge($this->likeableInput(), ['user_id' => $user->id]));

        $this->actingAs($user);
        $this->assertDatabaseHas('likes', array_merge($this->likeableInput(), ['user_id' => $user->id]));

        $this->delete(route('unlike'), $this->likeableInput())
            ->assertRedirect()
            ->assertSessionHasNoErrors();

        $this->assertDatabaseMissing('likes',
            array_merge($this->likeableInput(), ['user_id' => $user->id]));
    }

    public function test_authenticated_user_cannot_like_the_same_item()
    {
        $user = User::factory()->create();
        Like::query()->insert(array_merge($this->likeableInput(), ['user_id' => $user->id]));

        $this->actingAs($user);

        $this->assertDatabaseHas('likes', array_merge($this->likeableInput(), ['user_id' => $user->id]));

        $this->post(route('like'), $this->likeableInput())
            ->assertForbidden();

        $this->assertDatabaseHas('likes', array_merge($this->likeableInput(), ['user_id' => $user->id]));
    }

    public function test_authenticated_user_cannot_unlike_a_not_liked_item()
    {
        $this->actingAs($this->anotherUser);

        $this->delete(route('unlike'), $this->likeableInput())
            ->assertForbidden();
    }

    public function test_authenticated_user_cannot_like_unlikeable_item()
    {
        $this->actingAs(User::factory()->create());

        $this->post(route('like'), $this->notLikeableInput())
            ->assertRedirect()
            ->assertSessionHasErrors(['likeable_type']);
    }

    public function test_authenticated_user_like_ajax()
    {
        $this->actingAs($this->anotherUser);

        $this->json('post', route('like'), $this->likeableInput(), ['HTTP_X-Requested-With' => 'XMLHttpRequest'])
            ->assertJsonStructure(['likes']);

        $this->assertDatabaseHas('likes', array_merge($this->likeableInput(), ['user_id' => $this->anotherUser->id]));
    }

    public function test_authenticated_user_unlike_ajax()
    {
        $this->actingAs($this->anotherUser);

        Like::query()->insert(array_merge($this->likeableInput(), ['user_id' => $this->anotherUser->id]));
        $this->assertDatabaseHas('likes', array_merge($this->likeableInput(), ['user_id' => $this->anotherUser->id]));

        $this->delete(route('unlike'), $this->likeableInput(), ['HTTP_X-Requested-With' => 'XMLHttpRequest'])
            ->assertJsonStructure(['likes']);

        $this->assertDatabaseMissing('likes',
            array_merge($this->likeableInput(), ['user_id' => $this->anotherUser->id]));
    }

    public function test_unauthenticated_user_cannot_like()
    {
        $this->post(route('like'), [])
            ->assertRedirect(route('login.authenticate'));
    }

    public function test_unauthenticated_user_cannot_unlike()
    {
        $this->post(route('unlike'), [])
            ->assertRedirect(route('login.authenticate'));
    }

    private function likeableInput()
    {
        return [
            'likeable_type' => get_class($this->postRecord),
            'likeable_id' => $this->postRecord->id,
        ];
    }

    private function notLikeableInput()
    {
        return [
            'likeable_type' => get_class($this->author),
            'likeable_id' => $this->author->id,
        ];
    }

    private function nonExistingLikeableInput()
    {
        return [
            'likeable_type' => 'App\\Models\\Post',
            'likeable_id' => -1,
        ];
    }

    private function nonExistingClassInput()
    {
        return [
            'likeable_type' => 'App\\Invalid\\Class',
            'likeable_id' => 1,
        ];
    }

    private function invalidModelClassInput()
    {
        return [
            'likeable_type' => LikeControllerTest::class,
            'likeable_id' => 1,
        ];
    }

    private function validation_provider()
    {
        return [
            'it fails if payload is empty' => [
                function () {
                    return [
                        ['likeable_type'],
                        [],
                    ];
                },
            ],
            'it fails if likeable_type is invalid class' => [
                function () {
                    return [
                        ['likeable_type'],
                        $this->nonExistingClassInput(),
                    ];
                },
            ],
            'it fails if likeable_type is invalid model class' => [
                function () {
                    return [
                        ['likeable_type'],
                        $this->invalidModelClassInput(),
                    ];
                },
            ],
            'it fails if likeable_id is not existing' => [
                function () {
                    return [
                        ['likeable_id'],
                        $this->nonExistingLikeableInput(),
                    ];
                },
            ],
        ];
    }
}
