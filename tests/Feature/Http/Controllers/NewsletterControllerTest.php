<?php

namespace Tests\Feature\Http\Controllers;

use App\Services\Newsletter;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Mockery\MockInterface;
use Tests\TestCase;

class NewsletterControllerTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function test_duplicate_email_to_newsletter_with_error()
    {
        $this->mock(
            Newsletter::class, function (MockInterface $mock) {
                $mock->shouldReceive('subscribe')
                ->andThrow(new \Exception())
                ->once();
            });

        $this->post(route('newsletter'), [
            'email' => $this->faker->safeEmail(),
        ])
            ->assertRedirect()
            ->assertSessionHasErrors(['email']);
    }

    public function test_subscribe_email_to_newsletter()
    {
        $this->mock(
            Newsletter::class, function (MockInterface $mock) {
                $mock->shouldReceive('subscribe')->once();
            });

        $this->post(route('newsletter'), [
            'email' => $this->faker->safeEmail(),
        ])
            ->assertRedirect()
            ->assertSessionHasNoErrors();
    }

    /**
     * @dataProvider validation_provider
     */
    public function test_newsletter_with_validation_error($getData)
    {
        [$fields, $payload] = $getData();

        $this->post(route('newsletter'), $payload)
            ->assertRedirect()
            ->assertSessionHasErrors($fields);
    }

    private function validation_provider()
    {
        return [
            'it fails if payload is empty' => [
                function () {
                    return [
                        ['email'],
                        [],
                    ];
                },
            ],
            'it fails if email is empty string' => [
                function () {
                    return [
                        ['email'],
                        ['email' => ''],
                    ];
                },
            ],
            'it fails if email is not a valid email' => [
                function () {
                    return [
                        ['email'],
                        ['email' => 'not-a-valid-email'],
                    ];
                },
            ],
            'it fails if email is array' => [
                function () {
                    return [
                        ['email'],
                        ['email' => []],
                    ];
                },
            ],
        ];
    }
}
