<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class PostControllerTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function test_show_post_index()
    {
        $user = User::factory()->create();
        $posts = Post::factory()->count(5)->create([
            'user_id' => $user->id,
        ]);

        $response = $this->get(route('home'))
            ->assertOk()
            ->assertViewIs('posts.index');

        foreach ($posts as $post) {
            $response->assertSeeText($post->title);
        }
    }

    public function test_do_not_show_unpublished_post()
    {
        $user = User::factory()->createOne();
        $posts = Post::factory(5)->unPublished()->create([
            'user_id' => $user->id,
        ]);

        $response = $this->get(route('home'))
            ->assertOk();

        foreach ($posts as $post) {
            $response->assertDontSeeText($post->title);
        }
    }

    public function test_searchable_post_title()
    {
        $author = User::factory()->create([
            'username' => 'admin',
        ]);

        $title = 'Searchable title';

        Post::factory()->create([
            'user_id' => $author->id,
            'title' => $title,
            'excerpt' => 'Lorem ipsum dolorum',
        ]);

        $this->get(route('home', ['search' => $title]))
            ->assertOk()
            ->assertViewIs('posts.index')
            ->assertSeeText($title);
    }

    public function test_searchable_post_excerpt()
    {
        $author = User::factory()->isAdmin()->createOne();

        $excerpt = 'Searchable excerpt';

        Post::factory()->create([
            'user_id' => $author->id,
            'title' => 'Lorem ipsum dolorum',
            'excerpt' => $excerpt,
        ]);

        $this->get(route('home', ['search' => 'searchable excerpt']))
            ->assertOk()
            ->assertViewIs('posts.index')
            ->assertSee('Searchable excerpt');
    }

    public function test_view_post()
    {
        $user = User::factory()->create([
            'username' => 'admin',
        ]);
        $post = Post::factory()->create([
            'user_id' => $user->id,
        ]);

        $this->get(route('post.show', $post))
            ->assertOk()
            ->assertSessionHasNoErrors()
            ->assertViewIs('posts.show');
    }

    public function test_authorized_user_can_view_the_admin_list_of_posts()
    {
        $this->actingAsAuthorizedUser();

        $this->get(route('admin.posts.index'))
            ->assertOk()
            ->assertViewIs('admin.posts.index');
    }

    public function test_authorized_user_can_go_to_the_create_post_page()
    {
        $this->actingAsAuthorizedUser();

        $this->get(route('admin.posts.create'))
            ->assertOk()
            ->assertViewIs('admin.posts.create');
    }

    public function test_authorized_user_can_go_to_the_edit_page_of_a_post()
    {
        $user = $this->actingAsAuthorizedUser();

        $post = Post::factory()->create([
            'user_id' => $user->id,
        ]);

        $this->get(route('admin.posts.edit', $post))
            ->assertOk()
            ->assertViewIs('admin.posts.edit');
    }

    /**
     * @dataProvider validation_provider
     */
    public function test_authorized_user_create_post_with_validation_errors($getData)
    {
        $this->actingAsAuthorizedUser();

        [$fields, $payload] = $getData();

        $this->post(route('admin.posts.store'), $payload)
            ->assertRedirect()
            ->assertSessionHasErrors($fields);
    }

    /**
     * @dataProvider valid_post_provider
     */
    public function test_authorized_user_add_a_post($getData)
    {
        $this->actingAsAuthorizedUser();

        [$payload] = $getData();

        $this->post(route('admin.posts.store'), $payload)
            ->assertRedirect()
            ->assertSessionHasNoErrors();

        $this->assertDatabaseHas('posts', ['title' => $payload['title']]);
    }

    /**
     * @dataProvider valid_update_post_data
     */
    public function test_authorized_user_can_update_post($getData)
    {
        $user = $this->actingAsAuthorizedUser();

        $post = Post::factory()->createOne([
            'user_id' => $user->id,
        ]);
        $categories = Category::factory(5)->create();
        $post->categories()->attach($categories->pluck('id')->toArray());

        [$fields, $payload, $callback] = $getData();

        $this->patch(route('admin.posts.update', $post), $payload)
            ->assertRedirect()
            ->assertSessionHasNoErrors();

        $updatedPost = Post::find($post->id);

        foreach ($fields as $field) {
            switch ($field) {
                case 'categories' :
                    $this->assertEqualsCanonicalizing($payload[$field],
                        $updatedPost->categories->pluck('id')->toArray());
                    break;

                case 'thumbnail' :
                    if ($post->thumbnail !== '/images/illustration-1.png') {
                        Storage::disk('thumbnails')->assertMissing($post->thumbnail);
                        $this->assertEquals($payload[$field], $updatedPost->$field);
                    }

                    $callback($updatedPost);
                    break;

                default:
                    $this->assertEquals($payload[$field], $updatedPost->$field);
            }
        }
    }

    public function test_authorized_user_can_unpublish_post()
    {
        $admin = $this->actingAsAuthorizedUser();

        $post = Post::factory()->create([
            'user_id' => $admin->id,
        ]);
        $categories = Category::factory()->count(3)->create();
        $post->categories()->attach($categories->pluck('id')->toArray());
        $this->assertEquals(true, $post->isPublished());

        $this->patch(route('admin.posts.update', $post), [
            'title' => $post->title,
            'excerpt' => $post->excerpt,
            'body' => $post->body,
            'categories' => $categories->pluck('id')->toArray(),
            'is_published' => 0,
        ])
            ->assertRedirect()
            ->assertSessionHasNoErrors();

        $updatedPost = Post::find($post->id);

        $this->assertEquals(false, $updatedPost->isPublished());
    }

    public function test_authorized_user_can_republish_post()
    {
        $admin = $this->actingAsAuthorizedUser();

        $post = Post::factory()->unPublished()->create([
            'user_id' => $admin->id,
        ]);
        $categories = Category::factory()->count(3)->create();
        $post->categories()->attach($categories->pluck('id')->toArray());
        $this->assertEquals(false, $post->isPublished());

        $this->patch(route('admin.posts.update', $post), [
            'title' => $post->title,
            'excerpt' => $post->excerpt,
            'body' => $post->body,
            'categories' => $categories->pluck('id')->toArray(),
            'is_published' => 1,
        ])
            ->assertRedirect()
            ->assertSessionHasNoErrors();

        $updatedPost = Post::find($post->id);

        $this->assertEquals(true, $updatedPost->isPublished());
    }

    public function test_authorized_user_cannot_republish_post_if_already_published()
    {
        $admin = $this->actingAsAuthorizedUser();

        $post = Post::factory()->create([
            'user_id' => $admin->id,
        ]);
        $categories = Category::factory()->count(3)->create();
        $post->categories()->attach($categories->pluck('id')->toArray());
        $this->assertEquals(true, $post->isPublished());

        $this->patch(route('admin.posts.update', $post), [
            'title' => $post->title,
            'excerpt' => $post->excerpt,
            'body' => $post->body,
            'categories' => $categories->pluck('id')->toArray(),
            'is_published' => 1,
        ])
            ->assertRedirect()
            ->assertSessionHasNoErrors();

        $updatedPost = Post::find($post->id);

        $this->assertEquals(true, $updatedPost->isPublished());
        $this->assertEquals($post->published_at, $updatedPost->published_at);
    }

    public function test_authorized_user_can_delete_post()
    {
        $user = $this->actingAsAuthorizedUser();
        $post = Post::factory()->create([
            'user_id' => $user->id,
        ]);

        $this->from(route('admin.posts.index'))
            ->delete(route('admin.posts.destroy', $post))
            ->assertRedirect(route('admin.posts.index'));
    }

    public function test_user_cannot_see_unpublish_post()
    {
        $admin = User::factory(1)->isAdmin()->createOne();
        $post = Post::factory()->unPublished()->create([
            'user_id' => $admin->id,
        ]);
        $categories = Category::factory()->count(3)->create();
        $post->categories()->attach($categories->pluck('id')->toArray());
        $this->assertEquals(false, $post->isPublished());

        $this->get(route('post.show', $post))
            ->assertNotFound();
    }

    public function test_unauthorized_user_cannot_view_the_admin_list_of_posts()
    {
        $this->actingAsUnauthorizedUser();

        $this->get(route('admin.posts.index'))
            ->assertForbidden();
    }

    public function test_unauthorized_user_cannot_go_to_the_create_post_page()
    {
        $this->actingAsUnauthorizedUser();

        $this->get(route('admin.posts.create'))
            ->assertForbidden();
    }

    public function test_unauthorized_user_cannot_go_to_the_edit_post_page()
    {
        $this->actingAsUnauthorizedUser();

        $this->get(route('admin.posts.create'))
            ->assertForbidden();
    }

    public function test_unauthorized_user_cannot_delete_the_post()
    {
        $author = User::factory()->create();
        $post = Post::factory()->create([
            'user_id' => $author->id,
        ]);

        $this->actingAsUnauthorizedUser();

        $this->delete(route('admin.posts.destroy', $post))
            ->assertForbidden();
    }

    public function test_unauthenticated_user_cannot_view_the_admin_list_of_posts()
    {
        $this->get(route('admin.posts.index'))
            ->assertRedirect(route('login.authenticate'));
    }

    public function test_unauthenticated_user_cannot_go_to_the_create_post_page()
    {
        $this->get(route('admin.posts.create'))
            ->assertRedirect(route('login.authenticate'));
    }

    public function test_unauthenticated_user_cannot_go_to_the_edit_post_page()
    {
        $author = User::factory()->create();
        $post = Post::factory()->create([
            'user_id' => $author->id,
        ]);

        $this->get(route('admin.posts.edit', $post))
            ->assertRedirect(route('login.authenticate'));
    }

    public function test_unauthenticated_user_cannot_delete_a_post()
    {
        $author = User::factory()->create();
        $post = Post::factory()->create([
            'user_id' => $author->id,
        ]);

        $this->delete(route('admin.posts.destroy', $post))
            ->assertRedirect(route('login.authenticate'));
    }

    private function valid_post_provider()
    {
        return [
            'it successfully creates a post' => [
                function () {
                    return [
                        $this->valid_post_data(),
                    ];
                },
            ],
            'it successfully creates a post with upload' => [
                function () {
                    Storage::fake();

                    return [
                        array_merge($this->valid_post_data(),
                            ['thumbnail' => UploadedFile::fake()->image('thumbnail.jpg')]),
                    ];
                },
            ],
        ];
    }

    private function valid_post_data()
    {
        $categories = Category::factory()->count(3)->create();
        $post = Post::factory()->makeOne()->getAttributes();

        return array_merge($post, ['categories' => $categories->pluck('id')->toArray()]);
    }

    private function validation_provider()
    {
        return [
            'it fails if payload is empty' => [
                function () {
                    return [
                        ['title', 'excerpt', 'body', 'categories'],
                        [],
                    ];
                },
            ],
            'it fails if title is an empty string' => [
                function () {
                    return [
                        ['title'],
                        array_merge($this->valid_post_data(), ['title' => '']),
                    ];
                },
            ],
            'it fails if title is an empty array' => [
                function () {
                    return [
                        ['title'],
                        array_merge($this->valid_post_data(), ['title' => []]),
                    ];
                },
            ],
            'it fails if title is an array' => [
                function () {
                    return [
                        ['title'],
                        array_merge($this->valid_post_data(), ['title' => ['key' => 'value']]),
                    ];
                },
            ],
            'it fails if excerpt is an empty string' => [
                function () {
                    return [
                        ['excerpt'],
                        array_merge($this->valid_post_data(), ['excerpt' => '']),
                    ];
                },
            ],
            'it fails if excerpt is an empty array' => [
                function () {
                    return [
                        ['excerpt'],
                        array_merge($this->valid_post_data(), ['excerpt' => []]),
                    ];
                },
            ],
            'it fails if excerpt is an array' => [
                function () {
                    return [
                        ['excerpt'],
                        array_merge($this->valid_post_data(), ['excerpt' => ['key' => 'value']]),
                    ];
                },
            ],
            'it fails if body is an empty string' => [
                function () {
                    return [
                        ['body'],
                        array_merge($this->valid_post_data(), ['body' => '']),
                    ];
                },
            ],
            'it fails if body is an empty array' => [
                function () {
                    return [
                        ['body'],
                        array_merge($this->valid_post_data(), ['body' => []]),
                    ];
                },
            ],
            'it fails if body is an array' => [
                function () {
                    return [
                        ['body'],
                        array_merge($this->valid_post_data(), ['body' => ['key' => 'value']]),
                    ];
                },
            ],
            'it fails if categories is an empty string' => [
                function () {
                    return [
                        ['categories'],
                        array_merge($this->valid_post_data(), ['categories' => '']),
                    ];
                },
            ],
            'it fails if body is not an array' => [
                function () {
                    return [
                        ['categories'],
                        array_merge($this->valid_post_data(), ['categories' => '1,2,3']),
                    ];
                },
            ],
            'it fails if a category is non-existing' => [
                function () {
                    Category::factory(5)->create();

                    return [
                        ['categories'],
                        array_merge($this->valid_post_data(), ['categories' => [6]]),
                    ];
                },
            ],
        ];
    }

    private function valid_update_post_data()
    {
        return [
            'it successfully updates the title' => [
                function () {
                    return [
                        ['title'],
                        ['title' => 'A new title'],
                        [],
                    ];
                },
            ],
            'it successfully updates the excerpt' => [
                function () {
                    return [
                        ['excerpt'],
                        ['excerpt' => 'A new excerpt'],
                        [],
                    ];
                },
            ],
            'it successfully updates the body' => [
                function () {
                    return [
                        ['body'],
                        ['body' => 'A new body'],
                        [],
                    ];
                },
            ],
            'it successfully updates the categories' => [
                function () {
                    $categories = Category::factory(2)->create();

                    return [
                        ['categories'],
                        ['categories' => $categories->pluck('id')->toArray()],
                        [],
                    ];
                },
            ],
            'it successfully updates the thumbnail' => [
                function () {
                    Storage::fake();

                    return [
                        ['thumbnail'],
                        ['thumbnail' => UploadedFile::fake()->image('thumbnail.jpg')],
                        function (Post $post) {
                            Storage::disk()->assertExists(substr($post->thumbnail, strlen('/storage/')));
                        },
                    ];
                },
            ],
        ];
    }
}
