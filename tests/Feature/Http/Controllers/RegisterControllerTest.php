<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RegisterControllerTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function test_register_displays_the_form()
    {
        $this->get(route('register.create'))
            ->assertStatus(200);
    }

    public function test_successful_registration()
    {
        $user = User::factory()->makeOne();

        $this->post(route('register.store'), [
            'name' => $user->name,
            'username' => $user->username,
            'email' => $user->email,
            'password' => $user->password,
        ])
            ->assertSessionHasNoErrors()
            ->assertRedirect(route('home'));

        $user = User::where('email', $user->email)->first();

        $this->assertNotNull($user);
        $this->assertAuthenticatedAs($user);
    }

    /**
     * @dataProvider validation_provider
     */
    public function test_registration_fails_due_to_validation_errors($getData)
    {
        [$fields, $payload] = $getData();

        $this->post(route('register.store'), $payload)
            ->assertRedirect()
            ->assertSessionHasErrors($fields);
    }

    public function test_registration_form_will_redirect_if_authenticated()
    {
        $this->actingAsAuthorizedUser();

        $this->get(route('register.create'))
            ->assertRedirect(route('home'));
    }

    private function validation_provider()
    {
        return [
            'it fails if payload is empty' => [
                function () {
                    return [
                        ['name', 'username', 'email', 'password'],
                        [],
                    ];
                },
            ],
            'it fails if name is empty string' => [
                function () {
                    return [
                        ['name'],
                        array_merge($this->valid_data(), ['name' => '']),
                    ];
                },
            ],
            'it fails if name is more than 255 characters' => [
                function () {
                    return [
                        ['name'],
                        array_merge($this->valid_data(), ['name' => str_repeat('a', 256)]),
                    ];
                },
            ],
            'it fails if username is empty string' => [
                function () {
                    return [
                        ['username'],
                        array_merge($this->valid_data(), ['username' => '']),
                    ];
                },
            ],
            'it fails if username is less than 3 characters' => [
                function () {
                    return [
                        ['username'],
                        array_merge($this->valid_data(), ['username' => str_repeat('a', 2)]),
                    ];
                },
            ],
            'it fails if username is more than 255 characters' => [
                function () {
                    return [
                        ['username'],
                        array_merge($this->valid_data(), ['username' => str_repeat('a', 256)]),
                    ];
                },
            ],
            'it fails if username is not unique' => [
                function () {
                    User::factory()->createOne([
                        'username' => 'unique_username',
                    ]);

                    return [
                        ['username'],
                        array_merge($this->valid_data(), ['username' => 'unique_username']),
                    ];
                },
            ],
            'it fails if email is empty string' => [
                function () {
                    return [
                        ['email'],
                        array_merge($this->valid_data(), ['email' => '']),
                    ];
                },
            ],
            'it fails if email is not a valid email' => [
                function () {
                    return [
                        ['email'],
                        array_merge($this->valid_data(), ['email' => 'not-a-valid-email']),
                    ];
                },
            ],
            'it fails if email is more than 255 characters' => [
                function () {
                    return [
                        ['name'],
                        array_merge($this->valid_data(),
                            ['name' => str_repeat('a', 256 - strlen('@email.com')).'@email.com']),
                    ];
                },
            ],
            'it fails if email is not unique' => [
                function () {
                    User::factory()->createOne([
                        'email' => 'unique@email.com',
                    ]);

                    return [
                        ['email'],
                        array_merge($this->valid_data(), ['email' => 'unique@email.com']),
                    ];
                },
            ],
            'it fails if password is empty string' => [
                function () {
                    return [
                        ['password'],
                        array_merge($this->valid_data(), ['password' => '']),
                    ];
                },
            ],
            'it fails if password is less than 7 characters' => [
                function () {
                    return [
                        ['password'],
                        array_merge($this->valid_data(), ['password' => str_repeat('a', 6)]),
                    ];
                },
            ],
        ];
    }

    private function valid_data()
    {
        return User::factory()->makeOne()->getAttributes();
    }
}
