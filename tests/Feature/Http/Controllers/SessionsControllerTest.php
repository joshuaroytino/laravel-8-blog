<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SessionsControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_login_displays_the_login_form()
    {
        $this->get(route('login.create'))
            ->assertOk()
            ->assertViewIs('sessions.create');
    }

    /**
     * @dataProvider validation_provider
     */
    public function test_login_validation_errors($getData)
    {
        [$fields, $payload] = $getData();

        $this->post(route('login.authenticate'), $payload)
            ->assertSessionHasErrors($fields);
    }

    /**
     * @dataProvider invalid_login_provider
     */
    public function test_login_invalid_credentials($getData)
    {
        [$fields, $payload] = $getData();

        $this->post('login', $payload)
            ->assertRedirect()
            ->assertSessionHasErrors($fields);
    }

    public function test_login_authenticates_and_redirects_to_home()
    {
        $user = User::factory()->create();

        $this->post('login', [
            'email' => $user->email,
            'password' => 'password',
        ])
            ->assertRedirect(route('home'));

        $this->assertAuthenticatedAs($user);
    }

    public function test_logout()
    {
        $user = User::factory()->create();

        $this->actingAs($user);
        $this->assertAuthenticatedAs($user);

        $this->post('logout')
            ->assertRedirect(route('home'));

        $this->assertGuest();
    }

    public function test_login_form_will_redirect_if_authenticated()
    {
        $user = User::factory()->create();

        $this->actingAs($user);
        $this->assertAuthenticatedAs($user);

        $this->get(route('login.create'))
            ->assertRedirect(route('home'));

        $this->assertAuthenticated();
    }

    private function validation_provider()
    {
        return [
            'it fails if payload is empty' => [
                function () {
                    return [
                        ['email', 'password'],
                        [],
                    ];
                },
            ],
            'it fails if email is not an email' => [
                function () {
                    return [
                        ['email'],
                        array_merge($this->validation_data(), ['email' => 'not-a-valid-email']),
                    ];
                },
            ],
            'it fails if email is an empty string' => [
                function () {
                    return [
                        ['email'],
                        array_merge($this->validation_data(), ['email' => '']),
                    ];
                },
            ],
            'it fails if email is an array' => [
                function () {
                    return [
                        ['email'],
                        array_merge($this->validation_data(), ['email' => []]),
                    ];
                },
            ],
            'it fails if email is more than 255 characters' => [
                function () {
                    return [
                        ['email'],
                        array_merge($this->validation_data(),
                            ['email' => $this->generateEmailWithLengthForValidation()]),
                    ];
                },
            ],
            'it fails if password is empty string' => [
                function () {
                    return [
                        ['password'],
                        array_merge($this->validation_data(), ['password' => '']),
                    ];
                },
            ],
            'it fails if password is an array' => [
                function () {
                    return [
                        ['password'],
                        array_merge($this->validation_data(), ['password' => []]),
                    ];
                },
            ],
        ];
    }

    private function validation_data()
    {
        $user = User::factory()->makeOne();

        return [
            'email' => $user->email,
            'password' => 'password',
        ];
    }

    private function invalid_login_provider()
    {
        return [
            'it fails if email is not-existing' => [
                function () {
                    return [
                        ['email'],
                        array_merge($this->valid_data(), ['email' => 'invalid@email.com']),
                    ];
                },
            ],
            'it fails if password is not correct' => [
                function () {
                    return [
                        ['email'],
                        array_merge($this->valid_data(), ['password' => 'invalid-password']),
                    ];
                },
            ],
        ];
    }

    private function valid_data()
    {
        $user = User::factory()->createOne([
            'email' => 'user@email.com',
        ]);

        return ['email' => $user->email, 'password' => 'password'];
    }
}
