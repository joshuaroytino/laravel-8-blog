<?php

namespace Tests\Feature\Http\Controllers;

use Tests\TestCase;

class UpdatesControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testUpdatePageCanBeSeen()
    {
        $response = $this->get(route('updates'));

        $response->assertStatus(200)
            ->assertViewIs('updates.index')
            ->assertSeeText('Updates');
    }
}
