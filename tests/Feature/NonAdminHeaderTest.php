<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class NonAdminHeaderTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function setUp(): void
    {
        parent::setUp();

        $nonAdminUser = User::factory(1)
            ->isNotAdmin()
            ->createOne();
        $this->actingAs($nonAdminUser);
    }

    public function testCannotViewDashboardLink()
    {
        $this->get('/')
            ->assertDontSee('Dashboard');
    }

    public function testCannotViewCreatePostLink()
    {
        $this->get('/')
            ->assertDontSee('New Post');
    }

    public function testCanViewLogoutLink()
    {
        $this->get('/')
            ->assertSee('Log Out');
    }
}
