<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function actingAsAuthorizedUser()
    {
        $user = User::factory()->isAdmin()->createOne();
        $this->actingAs($user);

        return $user;
    }

    public function actingAsUnauthorizedUser()
    {
        $user = User::factory()->isNotAdmin()->createOne();
        $this->actingAs($user);

        return $user;
    }

    public function generateEmailWithLengthForValidation($length = 255): string
    {
        return str_repeat('a', $length - strlen('@email.com')).'@email.com';
    }
}
