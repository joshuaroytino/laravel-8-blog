<?php

namespace Tests\Unit\Model;

use App\Models\Category;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    use RefreshDatabase;

    private $category;

    public function setUp(): void
    {
        parent::setUp();
        $this->category = Category::factory()->create();
    }

    public function test_category_relationship_to_post()
    {
        $this->assertInstanceOf(BelongsToMany::class, $this->category->posts());
    }
}
