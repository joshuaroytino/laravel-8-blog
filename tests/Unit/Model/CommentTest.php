<?php

namespace Tests\Unit\Model;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CommentTest extends TestCase
{
    use RefreshDatabase;

    private $post;

    private $comment;

    private $author;

    public function setUp(): void
    {
        parent::setUp();
        $this->author = User::factory()->create([
            'username' => 'admin',
        ]);
        $this->post = Post::factory()->create([
            'user_id' => $this->author->id,
        ]);
        $this->comment = Comment::factory()->create([
            'user_id' => $this->author->id,
            'post_id' => $this->post->id,
        ]);
    }

    public function test_comment_relationship_to_post()
    {
        $this->assertInstanceOf(BelongsTo::class, $this->comment->post());
    }

    public function test_comment_relationship_to_author()
    {
        $this->assertInstanceOf(BelongsTo::class, $this->comment->author());
    }
}
