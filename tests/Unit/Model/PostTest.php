<?php

namespace Tests\Unit\Model;

use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PostTest extends TestCase
{
    use RefreshDatabase;

    private $author;

    private $post;

    public function setUp(): void
    {
        parent::setUp();
        $this->author = User::factory()->create([
            'username' => 'admin',
        ]);
        $this->post = Post::factory()->create([
            'user_id' => $this->author->id,
        ]);
    }

    public function test_post_relationship_to_categories()
    {
        $this->assertInstanceOf(BelongsToMany::class, $this->post->categories());
    }

    public function test_post_relationship_to_author()
    {
        $this->assertInstanceOf(BelongsTo::class, $this->post->author());
    }

    public function test_post_relationship_to_comments()
    {
        $this->assertInstanceOf(HasMany::class, $this->post->comments());
    }
}
