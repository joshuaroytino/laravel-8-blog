<?php

namespace Tests\Unit\Model;

use App\Models\User;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

//use PHPUnit\Framework\TestCase; #Factory will not work if used. Replace with Tests/TestCase

class UserTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_user_has_many_posts()
    {
        $user = User::factory()->create([
            'username' => 'admin',
        ]);

        $this->assertInstanceOf(HasMany::class, $user->posts());
    }
}
